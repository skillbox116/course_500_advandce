/// <reference types="cypress" />
const PNG = require('pngjs').PNG;

describe('Test suite 1', () => {
    it('test_case_1', () => {
        const expectedProfessionCount = 162;

        cy.visit('http://skillbox.ru');
        cy.get('h3').contains('Профессии').siblings('a').find('span')
            .as('blockPref');
        cy.get('@blockPref').should('contain.text', expectedProfessionCount);
        cy.intercept('GET', '**/profession/**').as('profession');
        cy.get('@blockPref').click();
        cy.wait('@profession').then((intercept) => {
            expect(intercept.response.body.meta.total).to.eq(expectedProfessionCount);
        });
        cy.get('.courses-block > .courses-block__load').should('contain.text', `Ещё 10 профессий из ${expectedProfessionCount - 10}`);
    });

    it('test_case_2', () => {
        const expectedProfessionCount = 162;
        const expectedShowArtiles = 20;

        cy.visit('http://skillbox.ru');
        cy.get('h3').contains('Профессии').siblings('a').find('span')
            .as('blockPref');
        cy.get('@blockPref').click();

        cy.intercept('GET', '**/profession/?page=2**').as('profession');
        cy.get('.courses-block > .courses-block__load').click();
        cy.wait('@profession');
        cy.get('.courses-block > .courses-block__load').should('contain.text', `Ещё 10 профессий из ${expectedProfessionCount - expectedShowArtiles}`);
        cy.get('article.card').should('have.length', expectedShowArtiles);
    });

    it.only('test_case_3', () => {
        cy.visit('http://skillbox.ru');
        cy.get('.video-block').as('videoBlock');
        cy.get('@videoBlock').scrollIntoView();
        cy.get('@videoBlock').screenshot('before_play', { overwrite: true });
        cy.wait(200);
        cy.get('@videoBlock').screenshot('after_play', { overwrite: true });
        cy.readFile('./cypress/screenshots/1-getting-started/todo.spec.js/before_play.png', 'base64').then((expectedImg) => {
            cy.readFile('./cypress/screenshots/1-getting-started/todo.spec.js/after_play.png', 'base64').then((actualImg) => {
                const expectedImage = PNG.sync.read(Buffer.from(expectedImg, 'base64'));
                const actualImage = PNG.sync.read(Buffer.from(actualImg, 'base64'));
                expect(expectedImage).not.to.equal(actualImage);
            });
        });
    });
});

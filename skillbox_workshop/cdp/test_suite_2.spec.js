/// <reference types="webdriverio" />

describe('Test suite 2', () => {
    it('test_case_1', async () => {
        await browser.url('http://skillbox.ru');
        const blockProf = await $('.video-block');
        const expectedImg = await browser.takeElementScreenshot(blockProf.elementId);
        const actualImg = await browser.takeElementScreenshot(blockProf.elementId);
        expect(expectedImg).not.toEqual(actualImg);
    });
});

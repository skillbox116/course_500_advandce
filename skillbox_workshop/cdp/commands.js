class Commands {
    get professionBlockLocator() { return $('.//h3[contains(text(), "Профессии")]/following-sibling::a/span'); }

    get professionButtonNextLocator() { return $('.courses-block > .courses-block__load'); }

    async checkProfessionCount(expectedProfessionCount) {
        const text = await (await this.professionBlockLocator).getText();
        expect(text).toContain(expectedProfessionCount.toString());
    }

    async checkProfessionButtonText(expectedText) {
        const buttonProf = await this.professionButtonNextLocator;
        expect(await buttonProf.getText()).toContain(expectedText);
    }

    async waitForTotalCount(expectedUrl, expectedProfessionCount) {
        const puppeteer = await browser.getPuppeteer();
        const page = (await puppeteer.pages())[0];
        await page.waitForResponse(async (response) => {
            const url = await response.url();
            const body = await response.json();
            return url === expectedUrl
                   && body.meta.total === expectedProfessionCount;
        });
    }

    async waitForPageCountInRequest(expectedCount) {
        const puppeteer = await browser.getPuppeteer();
        const page = (await puppeteer.pages())[0];
        await page.waitForRequest(async (request) => {
            return (await request.url()).includes(`?page=${expectedCount}`);
        });
    }
}

module.exports = new Commands();

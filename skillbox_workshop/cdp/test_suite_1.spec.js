/// <reference types="webdriverio" />

const commands = require('./commands');

describe('Test suite 1', () => {
    it('test_case_1', async () => {
        const expectedProfessionCount = 162;
        await browser.url('http://skillbox.ru');

        commands.checkProfessionCount(expectedProfessionCount);
        await (await commands.professionBlockLocator).click();
        await commands.waitForTotalCount(
            'https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=10&type=profession',
            expectedProfessionCount,
        );
        await commands.checkProfessionButtonText(`Ещё 10 профессий из ${expectedProfessionCount - 10}`);
    });

    it.only('test_case_2', async () => {
        const expectedProfessionCount = 162;
        const expectedShowArtiles = 20;

        await browser.url('http://skillbox.ru');

        await (await commands.professionBlockLocator).click();

        await (await commands.professionButtonNextLocator).click();

        await commands.waitForPageCountInRequest(2);

        await browser.waitUntil(async () => {
            const articles = await $$('article.card');
            return articles.length === expectedShowArtiles;
        });
        await commands.checkProfessionButtonText(`Ещё 10 профессий из ${expectedProfessionCount - expectedShowArtiles}`);
    });
});

let page
const respondData = require('./respondData')

describe('CDP Protocol - Request', () => {
    beforeEach(async () => {
        const puppeteer = await browser.getPuppeteer()

        page = (await puppeteer.pages())[0]
        await page.setRequestInterception(true)

        page.on('request', (request) => {
            if (request.url().includes('profession')) {
                request.respond({
                    status: 200,
                    contentType: 'application/json',
                    body: JSON.stringify(respondData)
                })
            }
            request.continue()
        })
    // })

    it('skillbox - request', async () => {
        await browser.url('https://skillbox.ru/code');

        const elem = await $('*=Все направления')
        await elem.click()

        // await page.waitForRequest('https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=12')

        await page.waitForRequest((request) => {
            const isFind = request.url().includes('https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession')
            if (isFind) {
                const limit = new URLSearchParams(request.url()).get('limit')
                
                return limit == '10'
            }
        })
    })

    function sleep(ms) {
        return new Promise((resolve) => {
          setTimeout(resolve, ms);
        });
      }

    it.only('skillbox - request events', async () => {
        const requests = []
        await page.on('request', (request) => {
            if (request.url().includes('https://skillbox.ru/api'))
            requests.push(request)
        })

        await browser.url('https://skillbox.ru/code');
        
        const elem = await $('*=Все направления')
        await elem.click()
        await sleep(4000)
        
        await browser.debug()
        // await page.waitForRequest('https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=12')

        // await page.waitForRequest((request) => {
        //     const isFind = request.url().includes('https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession')
        //     if (isFind) {
        //         const limit = new URLSearchParams(request.url()).get('limit')
                
        //         return limit == '10'
        //     }
        // })
    })


    it.only('skillbox - mock response', async () => {
        await browser.url('https://skillbox.ru/code');
        
        const elem = await $('*=Все направления')
        await elem.click()
        
        await browser.debug()
    })
});


it('skillbox - request events', async () => {
    await browser.url('https://skillbox.ru/code');
    
    const elem = await $('*=Все направления')
    await elem.click()
    await browser.waitForResponse(
        url='https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession',
        body={
            "data": [{
                "id": 69,
            }]
        }
    )
})
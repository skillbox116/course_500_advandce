module.exports = {
    "data": [
        {
            "id": 69,
            "nomenclature_id": 69,
            "label": "Профессия",
            "type": "profession",
            "slug": "profession-graphdesigner",
            "direction_slug": "design",
            "level": "for novichkov",
            "with_employment": 1,
            "duration": {
                "count": 24,
                "label": "месяца"
            },
            "title": "Графический дизайнер",
            "href": "https://skillbox.ru/course/profession-graphdesigner/",
            "images": {
                "desktop": {
                    "image": "https://248006.selcdn.ru/LandGen/desktop_2f781972c2056d2991a77f83eb2231440ddffb83.png",
                    "webp": "https://248006.selcdn.ru/LandGen/desktop_2f781972c2056d2991a77f83eb2231440ddffb83.webp"
                },
                "mobile": {
                    "image": "https://248006.selcdn.ru/LandGen/phone_2f781972c2056d2991a77f83eb2231440ddffb83.png",
                    "webp": "https://248006.selcdn.ru/LandGen/phone_2f781972c2056d2991a77f83eb2231440ddffb83.webp"
                }
            },
            "background": "#47C293",
            "dark_mode": false,
            "autopayment": true
        },
    ],
    "links": {
        "first": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=1",
        "last": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=19",
        "prev": null,
        "next": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 19,
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=3",
                "label": "3",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=4",
                "label": "4",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=5",
                "label": "5",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=6",
                "label": "6",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=7",
                "label": "7",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=8",
                "label": "8",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=9",
                "label": "9",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=10",
                "label": "10",
                "active": false
            },
            {
                "url": null,
                "label": "...",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=18",
                "label": "18",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=19",
                "label": "19",
                "active": false
            },
            {
                "url": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession?page=2",
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "path": "https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession",
        "per_page": 10,
        "to": 10,
        "total": 182
    }
}




const WebSocket = require('ws')

let wsEndpoint = 'ws://127.0.0.1:9222/devtools/browser/be164c16-d2b9-4fe3-bd57-65652e9f1f96'


async function send_ws_message(ws, command) {
    console.log(`Request: ${JSON.stringify(command)}`)
    ws.send(JSON.stringify(command))

    return new Promise(resolve => {
        ws.on('message', function (text) {
            const response = JSON.parse(text)
            if (response.id == command.id) {
                console.log(`Response: ${JSON.stringify(response)}`)

                resolve(response)
            }
        })
    })
}


async function getSessionId(ws) {
    const targets = await send_ws_message(ws, {
        id: 1,
        method: 'Target.getTargets'
    })

    const pageTarget = targets.result.targetInfos.find(info => info.type === 'page')

    const sessionId = (await send_ws_message(ws, {
        id: 2,
        method: 'Target.attachToTarget',
        params: {
            targetId: pageTarget.targetId,
            flatten: true
        }
    })).result.sessionId
    return sessionId
}


async function goto(ws, sessionId, url) {
    await send_ws_message(ws, {
        id: 3,
        sessionId,
        method: 'Page.navigate',
        params: {
            url
        }
    })
}


async function main () {
    const ws = new WebSocket(wsEndpoint)

    await new Promise(resolve => ws.once('open', resolve))

    const sessionId = await getSessionId(ws)

    await goto(ws, sessionId, 'https://skillbox.ru')
}

main()
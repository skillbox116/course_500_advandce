exports.config = {
    specs: ['./test/specs/example.e2e.js'],
    automationProtocol: 'webdriver',
    exclude: [],
    path: '/wd/hub',
    maxInstances: 2,
    capabilities: [{
        browserName: 'chrome',
        'goog:chromeOptions': {
            args: ['--headless', '--disable-gpu'],
            }
    }],
    baseUrl: 'https://the-internet.herokuapp.com',
    // Default timeout for all waitFor* commands.
    waitforTimeout: 10000, // ms
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,

    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'debug',

    services: ['selenium-standalone'],
    // https://webdriver.io/docs/frameworks
    framework: 'mocha',
    // https://webdriver.io/docs/dot-reporter
    reporters: ['spec'],
    // http://mochajs.org/
    mochaOpts: {
        ui: 'bdd',
        timeout: 6000000
    }
}
// wordpress_logged_in_322d7a6b4aee8fd917e3810c1f6dd310
// avorobey%7C1630408603%7CuaOMvcyfvkhYMBfIk728wdoVQAipol7bMEST7O62BLr%7C8b3e1086b0895213cfcd80dae1991982e42619f3efb02df2b992d73ee7ada9d3

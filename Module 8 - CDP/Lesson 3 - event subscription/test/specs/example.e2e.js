describe('CDP Protocol - Events', () => {
    it('shoul get message from console via CDP', () => {
        browser.on('Runtime.consoleAPICalled', (message) => {
            console.log(`Get messge: ${message.args[0].value}`)
        })

        browser.url('https://skillbox.ru')
        browser.debug()
    })

    it.only('should get message from console via Puppeteer', async () => {
        const puppeteer = await browser.getPuppeteer()
        const page = (await puppeteer.pages())[0]

        // await page.on('console', (message) => {
        //     console.log(`Get messge: ${message.args()[0]}`)
        // })

        await browser.url('https://skillbox.ru')
        await browser.debug()
    })
});

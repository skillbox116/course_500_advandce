exports.config = {
    automationProtocol: "devtools",
    specs: ["./test/**/*.js"],
    exclude: [],
    maxInstances: 10,
    capabilities: [{ 
        browserName: "chrome",
        'wdio:devtoolsOptions': {
          headless: true
      }
      }],
    // logLevel: "debug",
    bail: 0,
    baseUrl: "",
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: ["devtools"],
    framework: "mocha",
    reporters: ["spec"],
    mochaOpts: {
      ui: "bdd",
      timeout: 60000,
    }
  };
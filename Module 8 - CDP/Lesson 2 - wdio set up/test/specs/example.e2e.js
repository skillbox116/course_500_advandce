describe('CDP Protocol', () => {
    it('should start browser via puppeteer in wdio', () => {
        browser.url('http://skillbox.ru')
    })

    it('should assert', () => {
        browser.url('http://skillbox.ru')
        $('*=Программирование').click()
        $('h1=Программирование').waitForExist()
    })

    it.only('via puppeteer', async () => {
        const puppeteer = await browser.getPuppeteer()
        const page = (await puppeteer.pages())[0]

        await page.goto('http://skillbox.ru')

        await (await $('*=Программирование')).click()
        await (await $('h1=Программирование')).waitForExist()
    })
});

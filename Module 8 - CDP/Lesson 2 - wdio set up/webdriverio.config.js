exports.config = {
    specs: ['./test/specs/example.e2e.js'],
    automationProtocol: 'devtools',
    exclude: [],
    path: '/wd/hub',
    maxInstances: 2,
    capabilities: [{
        browserName: 'chrome'
    }],
    baseUrl: 'https://the-internet.herokuapp.com',
    // Default timeout for all waitFor* commands.
    waitforTimeout: 10000, // ms
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,

    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'debug',

    services: ['devtools'],
    // https://webdriver.io/docs/frameworks
    framework: 'mocha',
    // https://webdriver.io/docs/dot-reporter
    reporters: ['spec'],
    // http://mochajs.org/
    mochaOpts: {
        ui: 'bdd',
        timeout: 6000000
    }
}

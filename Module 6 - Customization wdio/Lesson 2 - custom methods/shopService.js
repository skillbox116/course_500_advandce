class ShopService {
    constructor(options) {
        this.options = options
    }

    before() {
        browser.addLocatorStrategy('menu', (text) => {
            return document
            .evaluate(`//*[@id="menu-primary-menu"]//*[contains(text(),"${text}")]`, document, null, XPathResult.ANY_TYPE, null)
            .iterateNext()
        })
        browser.addLocatorStrategy('catalogItem', (text) => {
            return document
            .evaluate(`//*[contains(text(),"${text}")]`, document, null, XPathResult.ANY_TYPE, null)
            .iterateNext()
        })

        browser.addCommand('selectCatalogItem', (items_hierarchy) => {
            let catalogEl = browser.custom$("menu", "Каталог")
            browser.performActions([{
                type: 'pointer',
                id: 'id_1',
                parametrs: {pointerType: 'mouse'},
                actions: [
                    {type: 'pointerMove', duration:0, origin: catalogEl, x: 20, y: 0},
                ]
            }])
            for (let item of items_hierarchy.slice(0, items_hierarchy.length - 1)) {
                let el = browser.custom$("catalogItem", item)
                browser.performActions([{
                    type: 'pointer',
                    id: 'id_1',
                    parametrs: {pointerType: 'mouse'},
                    actions: [
                        {type: 'pointerMove', duration:0, origin: el, x: 20, y: 0},
                    ]
                }])
            }
            browser.custom$("catalogItem", items_hierarchy[items_hierarchy.length - 1]).click()
        })
    }
}

module.exports = {
    ShopService: ShopService
}
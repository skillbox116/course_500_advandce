const WDIOReporter = require('@wdio/reporter').default

class CustomReporter extends WDIOReporter{
    constructor(options) {
        super(options)
        this.options = options
    }

    onBeforeCommand(command) {
        if (command.endpoint) {
            console.log(`   ${command.endpoint}`)
        }
    }

    onTestStart(test) {
        console.log(`${test.title}`)
    }
}

module.exports = {
    CustomReporter: CustomReporter
}
describe('Click to element with custom locator', () => {
    before(()=> {
        browser.addLocatorStrategy('menubar', (text) => {
            // .//button[ancestor::div[@role="menubar"]]/span[contains(text(), "File")]
            return document
                .evaluate(`.//button[ancestor::div[@role="menubar"]]/span[contains(text(), "${text}")]`, document, null, XPathResult.ANY_TYPE, null)
                .iterateNext()

        })

        browser.addLocatorStrategy('group', (text) => {
            // .//button[ancestor::div[@role="menubar"]]/span[contains(text(), "File")]
            return document
                .evaluate(`.//button[ancestor::div[@role="menubar"]]/span[contains(text(), "${text}")]`, document, null, XPathResult.ANY_TYPE, null)
                .iterateNext()

        })
    })

    it('should click to element', () => {
        browser.url(`/tinymce`);

        browser.custom$('menubar', 'Edit').click()
        browser.custom$('menubar', 'File').click()
        browser.custom$('menubar', 'View').click()

        browser.custom$('group', 'Undo').click()
        browser.debug()
    })
});

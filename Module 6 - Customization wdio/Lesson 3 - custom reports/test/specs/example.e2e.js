const { addFeature } = require('@wdio/allure-reporter').default

describe('Move sliders point', () => {
    before(() => {
        addFeature('Custom Skillbox feature')
        browser.addCommand('moveSliderToPoint', (expectedPoint) => {
            const draggable = $('input[type="range"]')
            const startPoint = -64
            const sliderStep = 15

            for (let currentPosition = startPoint; currentPosition <= 129;) {
                browser.performActions([{
                    type: 'pointer',
                    id: 'id_1',
                    parametrs: {pointerType: 'mouse'},
                    actions: [
                        {type: 'pointerMove', duration:0, origin: draggable, x: currentPosition, y: 0},
                        {type: 'pointerDown', button: 0},
                        {type: 'pause', duration:10},
                        {type: 'pointerMove', duration:0, origin: draggable, x: currentPosition + sliderStep, y: 0},
                        {type: 'pointerUp', button: 0}
                    ]
                }])
                let value = $('#range').getText()
                // добавим здесь  return
                if (value == expectedPoint) return
                currentPosition += sliderStep
            }
            // а здесь выбросим ошибку
            throw new Error(`Point ${expectedPoint} not found`)
        })
    })
    it('should click to element', () => {
        browser.url(`/horizontal_slider`);
        browser.moveSliderToPoint("2.5")
        // browser.debug()
    })

    it('should click to element - 2', () => {
        browser.url(`/horizontal_slider`);
        browser.moveSliderToPoint("6.5")
        // browser.debug()
    })
});

const WDIOReporter = require('@wdio/reporter').default

class CustomReporter extends WDIOReporter{
    constructor(options) {
        super(options)
        this.options = options
    }

    onBeforeCommand(command) {
        console.log()
    }

    onAfterCommand(command) {
        console.log()
    }

    onTestStart(test) {
        console.log(`${test.title} was started.`)
    }

    onSuiteEnd(suite) {
        const status_symbols = {
            passed: '✓',
            failed: '✖'
        }
        if (this.options.disableColor) {
            
        }
        this.write(`\x1b[94m ${suite.title} \n`)
        for (const test of suite.tests) {
            if (test.state == 'passed') {
                this.write(`    \x1b[32m ${status_symbols.passed} ${test.title} (${test.duration / 1000})s \n`)
            } else {
                this.write(`    \x1b[31m ${status_symbols.failed} ${test.title} (${test.duration / 1000})s \n`)
            }
        }
    }
}

module.exports = {
    CustomReporter: CustomReporter
}
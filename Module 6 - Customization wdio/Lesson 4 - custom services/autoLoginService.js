class AutoLoginService {
    constructor(options) {
        this.options = options
        this.withAutorized = false
    }

    before() {
        browser.overwriteCommand('url', (originUrlFunc, url, withAutorize = false) => {
            originUrlFunc(url)
            if (withAutorize) {
                browser.setCookies(this.options.defaultUser)
                browser.refresh()
                this.withAutorized = true
            }
        })
    }

    afterTest() {
        if (this.withAutorized) {
            browser.deleteCookies([this.options.defaultUser.name])
            this.withAutorized = false
        }
    }
}

module.exports = {
    AutoLoginService: AutoLoginService
}
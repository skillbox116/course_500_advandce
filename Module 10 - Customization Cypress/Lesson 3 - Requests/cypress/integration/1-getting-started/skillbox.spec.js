/// <reference types="cypress" />
describe('Skillbox', () => {
    it('Wait to request', ()=> {
        cy.visit('https://skillbox.ru/code/')

        cy.intercept('GET', '**/course/**').as('apiCourses')
        cy.get('input[placeholder="Поиск"]').type('тестирование{enter}')

        cy.wait('@apiCourses').then((interception) => {
            const params = new URLSearchParams(interception.request.url)
            expect(params.get('search')).to.be.equal('тестирование')
        })
    })

    it.only('mocking', () => {
        cy.visit('https://skillbox.ru/code/')
        
        cy.intercept('GET', '**/course/**', {fixture: 'courses'}).as('apiCourses')
        
        cy.get('input[placeholder="Поиск"]').type('тестирование{enter}')
    })
})
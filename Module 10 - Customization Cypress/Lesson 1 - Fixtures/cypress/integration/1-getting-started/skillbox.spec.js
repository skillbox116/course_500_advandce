/// <reference types="cypress" />
describe('Skillbox', () => {
    it('use fixtures', () => {
        cy.visit('https://www.google.com/')

        cy.fixture('courses').then((data) => {
            cy.get('input[name="q"]').type(data.courses[0])
            cy.get('input[name="q"]').clear().type(data.courses[1])
        })
    })
    it('import', () => {
        cy.visit('https://www.google.com/')
        const data = require('./data');
        console.log(data.courses)
        data.courses = []
        console.log(data.courses)

        cy.fixture('courses').then((data) => {
            data.courses[0] = 'Java'
            cy.get('input[name="q"]').type(data.courses[0])
        })

        cy.fixture('courses').then((data) => {
            cy.get('input[name="q"]').clear().type(data.courses[0])
        })
    })

    it.only('from file', () => {
        cy.writeFile('cypress/fixtures/newFile.json', {"courses": ["javascript", "C#"]})
        cy.visit('https://www.google.com/')

        cy.fixture('newFile').should((data) => {
            expect(data.courses[0]).to.equal('javascript')
        })
    })
})
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('login', (username='admin', password='admin') => { 
    cy.get('#username').type(username)
    cy.get('#password').type(password)
    cy.get('button').click()
    cy.contains('#flash', 'invalid').should('be.visible')
})

Cypress.Commands.add('typeByLabel', { prevSubject: true }, (subject, username='admin') => { 
    cy.get(subject).type(username)
})


Cypress.Commands.add('custom$', (strategyName, value) => {
    switch (strategyName) {
        case 'byLabel':
            return cy.get(`label[for="${value}"] ~ input`)
        default:
            return undefined
    }
})

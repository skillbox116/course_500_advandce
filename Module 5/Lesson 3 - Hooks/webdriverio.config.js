
const hooks = require('./hooks');

exports.config = {
    specs: ['./test/specs/example.e2e.js'],
    automationProtocol: 'webdriver',
    exclude: [],
    path: '/wd/hub',
    maxInstances: 2,
    capabilities: [{
        maxInstances: 2,
        browserName: 'chrome',
        'goog:chromeOptions': {
            args: ['--headless']
        }
    }],
    baseUrl: 'https://the-internet.herokuapp.com',
    // Default timeout for all waitFor* commands.
    waitforTimeout: 3000, // ms
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,

    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'debug',

    services: ['selenium-standalone'],
    // https://webdriver.io/docs/frameworks
    framework: 'mocha',
    // https://webdriver.io/docs/dot-reporter
    reporters: ['spec'],
    // http://mochajs.org/
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
    ...hooks,
}

describe('Dynamic loading', () => {
    before(async () => {
        await browser.setTimeout({ pageLoad: 10000, 'implicit': 6000 })
    })

    beforeEach(async () => {
        await browser.url('/dynamic_loading/2')

        const elemStart = await $('#start > button')
        await elemStart.click()
    })

    it('should wait via implicit', async () => {
        const elemFinish = await $('div#finish')
        expect(elemFinish.error).toBe(1)
    })

    it('should wait via waitUntil', async () => {
        const elemFinish = await $('div#finish')
        await elemFinish.waitUntil(function () {
            return this.isDisplayed()
        }, {timeout: 6000})
    })
});

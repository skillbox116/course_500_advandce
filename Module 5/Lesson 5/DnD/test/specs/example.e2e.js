describe('Drag and drop', () => {
    it.only('should work drag and drop', () => {
        browser.url('https://jqueryui.com/droppable/')

        const iframe = $('iframe.demo-frame')
        iframe.waitForExist()
        browser.switchToFrame(iframe)

        const draggable = $('#draggable')
        const droppable = $('#droppable')
        draggable.waitForDisplayed()
        droppable.waitForDisplayed()

        // draggable.dragAndDrop(droppable)

        browser.performActions([{
            type: 'pointer',
            id: 'id_1',
            parametrs: {pointerType: 'mouse'},
            actions: [
                {type: 'pointerMove', duration: 0, origin: draggable, x: 0, y: 0},
                {type: 'pointerDown', button: 0},
                {type: 'pause', duration: 30},
                {type: 'pointerMove', duration: 0, origin: droppable, x: 0, y: 0},
                {type: 'pointerUp', button: 0},
            ]
        }])

        browser.debug()
    });
});

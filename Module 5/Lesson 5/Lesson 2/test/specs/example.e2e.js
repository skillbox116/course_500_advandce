function fillField(obj) {
    browser.execute((obj) => {
        document.querySelector(obj.locator).value = obj.value
    }, obj)
}

describe('inject js to app', () => {
    it('should inject js to page', () => {
        browser.url(`/login`);
        
        fillField({locator: '#username', value: 'username'})
        fillField({locator: '#password', value: 'password'})

        browser.execute(() => {
            document.querySelector('button').click()
        })
        browser.debug()
    })
});

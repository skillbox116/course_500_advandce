describe('Dynamic loading', () => {
    it('should wait via waitUntil', async () => {
        await browser.setTimeout({ pageLoad: 10000, 'implicit': 6000 })

        await browser.url('/dynamic_loading/2')

        const elemStart = await $('#start > button')
        await elemStart.click()

        const elemFinish = await $('div#finish')
        await elemFinish.waitUntil(function () {
            return this.isDisplayed()
        }, {timeout: 6000})
    })
});

describe('Dynamic loading', () => {
    it('should wait via implicit', async () => {
        await browser.setTimeout({ pageLoad: 10000, 'implicit': 6000 })

        await browser.url('/dynamic_loading/2')

        const elemStart = await $('#start > button')
        await elemStart.click()

        const elemFinish = await $('div#finish')
        expect(elemFinish.error).toBe(undefined)
    })
});

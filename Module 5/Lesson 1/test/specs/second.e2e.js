describe('Dynamic loading', () => {
    it('should wait via waitFor', async () => {
        await browser.url('/dynamic_loading/1')

        const elemStart = await $('#start > button')
        await elemStart.click()

        const elemFinish = await $('div#finish')
        await elemFinish.waitForDisplayed({timeout: 6000})
    })
});

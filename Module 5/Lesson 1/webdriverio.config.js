exports.config = {
    suites: {
        login: ['./test/specs/example.e2e.js'], // 1 thread
        cart: [
            './test/specs/second.e2e.js',
            './test/specs/finish.e2e.js'
        ], // 2 thread
    },
    automationProtocol: 'webdriver',
    exclude: [],
    path: '/wd/hub',
    maxInstances: 2,
    capabilities: [{
        browserName: 'chrome',
    }],
    baseUrl: 'https://the-internet.herokuapp.com',
    // Default timeout for all waitFor* commands.
    waitforTimeout: 3000, // ms
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,

    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'debug',

    services: ['selenium-standalone'],
    // https://webdriver.io/docs/frameworks
    framework: 'mocha',
    // https://webdriver.io/docs/dot-reporter
    reporters: ['spec'],
    // http://mochajs.org/
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
    
}

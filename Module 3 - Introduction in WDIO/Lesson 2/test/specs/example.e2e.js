describe('My Login application', () => {
    it('should login with valid credentials', async  () => {
        await browser.url(`/login`);
        let elementUsername = await $('#username')
        await elementUsername.setValue('username')

        let elementPassword = await $('#password')
        await elementPassword.setValue('password')
        await expect(elementPassword).toBeFocused()


        let elementLogin = await $('button[type="submit"]')
        console.log(await elementLogin.getAttribute('class'))
        await elementLogin.click()

        let elementError = await $('#flash')
        await expect(elementError).toHaveTextContaining('Your username is invaliasdasdd!')
    })
});


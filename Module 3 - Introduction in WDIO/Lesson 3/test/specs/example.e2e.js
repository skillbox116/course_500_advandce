// describe -> Test suite
// it -> Test case
describe('My Login application', () => {

    it('should login with valid credentials', async () => {
        await browser.url(`/login`);
        // await browser.execute(() => {
        //     document.querySelector('#username').value = 'username'
        //     document.querySelector('#password').value = 'username'
        //     document.querySelector('button[type="submit"]').click()
        // })
        // await 
        // await browser.pause()
        let elemUsername = await $('#username')
        console.log(await elemUsername.getAttribute('type'))
        await elemUsername.setValue('username')


        let elemPassword = await $('#password')
        await elemPassword.setValue('password')
        expect(elemPassword).toBeFocused()


        let elemButton = await $('button[type="submit"]')
        await elemButton.click()
        await browser.pause()
        await (await $('//div[@class="flash error"]')).waitUntil(async function () {
            console.log(await this.getText())
            expect(await this.getText()).toBe('Your username is invalid!\n×')
            return await this.getText() == (`Your username is invalid!\n×`)
        })
    });
});

const commands = require('./commands');

describe('Open new tabs', () => {
    it('should open a new tab', async () => {
        const expectedCount = 162;

        await browser.url('https://skillbox.ru/');
        const component = await $('.//h3[contains(text(), "Профессии")]/following-sibling::a/span');
        const text = await component.getText();
        expect(text).toContain(expectedCount.toString());
        await component.click();
        const puppeteer = await browser.getPuppeteer();
        const page = (await puppeteer.pages())[0];
        await page.waitForResponse(
            url = 'https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=10&type=profession',
            body = {
                meta: [{
                    total: 162,
                }],
            },
        );

        const button = await $('section.courses-block > button.courses-block__load');
        const button_text = await button.getText();
        expect(button_text).toContain(`Ещё 10 профессий из ${expectedCount - 10}`);
        await button.click();
        await browser.waitUntil(async () => {
            const articles = await $$('article.card');
            return articles.length === 20;
        });
    });

    it.only('should open a new tab - 2', async () => {
        const expectedCount = 162;

        await browser.url('https://skillbox.ru/');
        commands.checkProfessionCount(expectedCount);
        await (await commands.professionBlockLocator).click();

        await commands.waitForResponse(
            'https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=10&type=profession',
            {
                meta: [{
                    total: 162,
                }],
            },
        );
        await commands.checkProfessionsButtonText(`Ещё 10 профессий из ${expectedCount - 10}`);
        await (await commands.professionsButtonNext).click();
        await commands.waitForArticleCount(20);
    });
});

// async (response) => {
//     return await response.url() === 'https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=10&type=profession'
//     && (await response.json()).meta.total === expectedProfessionCount;
// }

// let page
// const respondData = require('./respondData')

// describe('CDP Protocol - Request', () => {
//     beforeEach(async () => {
//         const puppeteer = await browser.getPuppeteer()

//         page = (await puppeteer.pages())[0]
//         await page.setRequestInterception(true)

//         page.on('request', (request) => {
//             if (request.url().includes('profession')) {
//                 request.respond({
//                     status: 200,
//                     contentType: 'application/json',
//                     body: JSON.stringify(respondData)
//                 })
//             }
//             request.continue()
//         })
//     })

//     it('skillbox - request', async () => {
//         await browser.url('https://skillbox.ru/code');

//         const elem = await $('*=Все направления')
//         await elem.click()

//         // await page.waitForRequest('https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=12')

//         await page.waitForRequest((request) => {
//             const isFind = request.url().includes('https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession')
//             if (isFind) {
//                 const limit = new URLSearchParams(request.url()).get('limit')

//                 return limit == '10'
//             }
//         })
//     })

//     function sleep(ms) {
//         return new Promise((resolve) => {
//           setTimeout(resolve, ms);
//         });
//       }

//     it('skillbox - request events', async () => {
//         const requests = []
//         await page.on('request', (request) => {
//             if (request.url().includes('https://skillbox.ru/api'))
//             requests.push(request)
//         })

//         await browser.url('https://skillbox.ru/code');

//         const elem = await $('*=Все направления')
//         await elem.click()
//         await sleep(4000)

//         await browser.debug()
//         // await page.waitForRequest('https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession/?page=1&limit=12')

//         // await page.waitForRequest((request) => {
//         //     const isFind = request.url().includes('https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession')
//         //     if (isFind) {
//         //         const limit = new URLSearchParams(request.url()).get('limit')

//         //         return limit == '10'
//         //     }
//         // })
//     })

//     it.only('skillbox - mock response', async () => {
//         await browser.url('https://skillbox.ru/code');

//         const elem = await $('*=Все направления')
//         await elem.click()

//         await browser.debug()
//     })
// });

// it('skillbox - request events', async () => {
//     await browser.url('https://skillbox.ru/code');

//     const elem = await $('*=Все направления')
//     await elem.click()
//     await browser.waitForResponse(
//         url='https://skillbox.ru/api/v6/ru/sales/skillbox/directions/all/nomenclature/profession',
//         body={
//             "data": [{
//                 "id": 69,
//             }]
//         }
//     )
// })

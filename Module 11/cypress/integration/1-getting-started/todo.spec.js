describe('Test suite 1', () => {
    it('test_case_1', () => {
        const expectedProfessionCount = 162;
        cy.visit('https://skillbox.ru/');
        cy.get('h3').contains('Профессии').siblings('a')
            .as('blockProf');
        cy.get('@blockProf').find('span').should('contain.text', expectedProfessionCount);
        cy.intercept('GET', '**/profession/**').as('professions');
        cy.get('@blockProf').click();
        cy.wait('@professions').then((interception) => {
            expect(interception.response.body.meta.total).to.eq(expectedProfessionCount);
        });
        cy.get('.courses-block > .courses-block__load').should('contain.text', `Ещё 10 профессий из ${expectedProfessionCount - 10}`);
    });
});

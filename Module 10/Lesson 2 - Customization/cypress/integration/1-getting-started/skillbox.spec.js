/// <reference types="cypress" />
describe('Skillbox', () => {
    it('Custom command', ()=> {
        // перейдем на уже знакомый нам сайт с логином
        cy.visit('https://the-internet.herokuapp.com/login')

        // Далее напишем код, который будет логинется
        // cy.get('#username').type('admin')
        // cy.get('#password').type('admin')
        // cy.get('button').click()
        // cy.contains('#flash', 'invalid').should('be.visible')
        // и теперь давайте вынесем его в отдельный метод. 
        // Для этого перейдем в файл commands.

        // Метод отрабатывает как мы от него и ожилдали, при этом методу при необходимости можно передавать параметры
        // cy.login()

        // Теперь мы можем вызвать метод с аргументами
        // cy.login('Alex')

        // В данном случае указываем найденный нами элемент и внутри метода login теперь мы можем достучаться до найденного эл
        // cy.get('#login').login('Alex')

        cy.custom$('byLabel', 'username').type('admin')
        cy.custom$('byLabel', 'password').type('admin')
    })
})
/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  // все плагины лучше всего хрангить в диретории плагинов
  // описание плагина начинается с module.exports = (on, config)
  // в данном случае все максимално просто.
  // on - это аргумент, который предоставляет возможность реализации подписки на события
  // config - это весь конфиг cypress. Он только в режиме RO. 

  // Далее мы можем реализовать подписку на следующие события:
  // before:run
  // after:run
  // before:spec
  // after:spec
  // before:browser:launch
  // after:screenshot
  // file:preprocessor
  // task

   // давайте попробуем подписаться на события тестов.
   // Не будем с Вами писать что-то сложное, просто посмотрим как это реализовывается. 
   // Вся логика строится аналогично wdio
   on('after:spec', (spec, results) => {
    console.log(spec)
    console.log(results)
   })
   // Для запуска в режиме open нужно проставить флаг experimentalInteractiveRunEvents в true в конфиге
   // Мы с вами будем запускать через cypress run
   // И как мы видим в консоль распечатались данные теста и результаты.
   // На основе этих данных можно реализовывать репортеры, сендеры сообщений
   // или улучшать логику тестовых прогонов
   //<обратно в гуугл> 
}



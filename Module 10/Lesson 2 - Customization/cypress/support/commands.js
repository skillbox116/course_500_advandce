// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

// Введем команду для добавления нового метода. Назовем его login и передадим коллбэк функцию с действиями
// Cypress.Commands.add('login', () => {
//     // скопируем сюда всё что писали в тесте
//     // и выполним метод в тесте
//     cy.get('#username').type('admin')
//     cy.get('#password').type('admin')
//     cy.get('button').click()
//     cy.contains('#flash', 'invalid').should('be.visible')
// })

// для добавления аргументов, достаточно просто заполнить их в коллбеке
// Cypress.Commands.add('login', (username='admin', password='admin') => {
//     // и заиспользовать в действиях
//     cy.get('#username').type(username)
//     cy.get('#password').type(password)
//     cy.get('button').click()
//     cy.contains('#flash', 'invalid').should('be.visible')
// })

// Теперь давайте разберем второй аргумент, который передается в метод - options.
// Этот аргумент по факту сообщает сайпресу о том, что данный метод будет родительским или дочерним.
// Дочерний метод - это метод, который будет вызываться в цепочке вызовов всех методов.
// Например cy.get('').login() и чтобы у метода был доступ к найденному элементу нужно указать параметр
//   {
//     prevSubject: true,
//   } 
// Cypress.Commands.add('login',   { prevSubject: true,}, (subject, username='admin', password='admin') => {
//     // в свою очередь внутри метода мы можем использовать subject как найденный элемент и работать с ним как захотимю
//     cy.get(subject).get('#username').type(username)
//     cy.get(subject).get('#password').type(password)
//     cy.get(subject).get('button').click()
//     cy.contains('#flash', 'invalid').should('be.visible')
// })

// Не будем явно смотреть команду по переопределению метода, по факту он работает так же как add только нужно укаывать название уже существующего метода.

// Давайте рассмотрим тему с локаторами. Можем ли мы как-то изменить стратегию локаторв? 
// По факту в сайпрес нет такой возможности, как в wdio. То есть мы не можем укзаать cy.custom$ и установить альяс для упрощенного поиска селектора
// НО! никто нас не ограничивает в возможности написать кастомный метод, который позволит найти нужный нам элемент по собственным правилам
// Например мы можем написать метод который будет принимать нужную стратегию и искать по ней элемент
Cypress.Commands.add('custom$', (strategyName, value) => {
    switch (strategyName) {
        case 'byLabel': 
            // Например в нашем случае инпут, который лежит рядом с лейблом
            return cy.get(`label[for="${value}"]  ~ input`)
        default:
            return undefined
    }
    // собственно вокруг этого можно настраивать разную логику работы для того, чтобы упростить тестирование
})


 // И последнее что я предлагаю рассмореть в данном уроке это подключение плагинов.
 // На самом деле все очень похоже с тем как плагины работают в wdio.
 // 
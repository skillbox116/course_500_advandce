/// <reference types="cypress" />

describe('Skillbox', () => {
    beforeEach(() => {
        cy.visit('https://the-internet.herokuapp.com/login')
    })
    it('actions with element', () => {
        cy.get('input#username').type('username')

        cy.get('input#username').type('username').clear().click().type('username_2').closest('#login').as('username')
        cy.get('@username').find('input')

        cy.get('input').each((el) => {
            console.log(el.attr('id'))
            cy.get(el).type('username')
        })

        cy.get('input#username').invoke('attr', 'name', 'not_username')
    })

    it('trigger', () => {
        cy.visit('https://the-internet.herokuapp.com/horizontal_slider')

        cy.get('[type="range"]').invoke('val', 2.5).trigger('change').get('input[type=range]').siblings('span').should('have.text', '2.5')
    })
})
/// <reference types="cypress" />

describe('Skillbox', () => {

    beforeEach(() => {
        cy.visit('https://the-internet.herokuapp.com/login')
        cy.get('label[for="username"]').as('labelUsername')
    })
    it('try test skillbox site', () => {
        cy.get('input').each((el) => {
            cy.get(el).type('username')
        })

        cy.get('@labelUsername').siblings('input')

        cy.get('@labelUsername').closest('#login').find('input#password')

        cy.contains('Username')
    })
})
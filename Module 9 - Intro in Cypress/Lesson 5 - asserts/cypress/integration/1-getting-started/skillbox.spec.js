/// <reference types="cypress" />

describe('Skillbox', () => {
    it('asserts with element', () => {
        cy.visit('https://the-internet.herokuapp.com/login')

        cy.get('input#username').as('username')

        cy.get('@username').type('username').should('have.value', 'username')

        cy.get('input').should((els) => {
            expect(els[0]).to.have.attr('id')
            expect(els).to.have.length(2)
            assert.equal(els.length, 2, 'vals equal')
        })

        
    })
})
const uuid = require('uuid');

describe('localstorage, cookies, sessionstorage', () => {
    it.only('localstorage, cookies, sessionstorage', () => {
        browser.url('/')

        browser.execute(() => {
            window.localStorage.setItem('login', 'avorobey')
            window.sessionStorage.setItem('login', 'avorobey')
        })
        
        const lValue = browser.execute(() => window.localStorage.getItem('login'))
        const sValue = browser.execute(() => window.sessionStorage.getItem('login'))

        expect(lValue).toEqual('avorobey')
        expect(sValue).toEqual('avorobey')

        browser.execute(() => {
            window.localStorage.removeItem('login')
        })

        const lValue_1 = browser.execute(() => window.localStorage.getItem('login'))
        expect(lValue_1).toBeFalsy()
    });
});

const uuid = require('uuid')

describe('Open new tabs', () => {
    it('should open a new tab', () => {
        browser.url('https://wall.sli.do/event/uxq42ad8?section=cbbd9046-6ff0-4a18-99f0-6b848138c53e')

        browser.newWindow('https://app.sli.do/event/uxq42ad8/live/questions', {
            windowName: 'Sli.do window'
        })

        const msg = `My new question ${uuid.v4()}`
        // 268fe82e-2c49-49f9-83cc-3269251ba2df

        const field = $('#question-field')
        field.click()
        field.setValue(msg)
        $('button=Send').click()

        

        let tabs = browser.getWindowHandles() // [1,2]

        browser.switchToWindow(tabs[0])

        $(`div=${msg}`).waitForExist()

        browser.switchToWindow(tabs[1])
        browser.closeWindow()

        let new_tabs = browser.getWindowHandles() // [1]
        expect(new_tabs.length).toBe(1)
        browser.debug()
    });

    it.only('should resize window', () => {
        browser.url('https://wall.sli.do/event/uxq42ad8?section=cbbd9046-6ff0-4a18-99f0-6b848138c53e')
        browser.setWindowSize(200, 300)
        browser.debug()
    })
});

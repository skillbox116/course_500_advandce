describe('Screenshots', () => {
    it('save browser screenshot', () => {
        browser.url('https://webdriver.io/')
        const { width, height } = browser.execute(() => {
            return {
                height: document.body.scrollHeight,
                width: document.body.scrollWidth
            }
        })
        browser.setWindowSize(width, height)
        browser.saveScreenshot('browser_screenshot.png')
        
    });

    it('save elemnt screenshot', () => {
        browser.url('https://webdriver.io/')
        $('a.button=Get Started').saveScreenshot('element_screenshot.png')
        expect(1).toBe(0)
    })

    it('compare element screenshot by Buffer', () => {
        browser.url('https://webdriver.io/')
        const screenshot_1 = $('a.button=Get Started').saveScreenshot('element_screenshot.png')
        const screenshot_2 = $('a.button=Support').saveScreenshot('element_screenshot.png')
        expect(screenshot_1).toEqual(screenshot_2)
    })

    it.only('compare element screenshot without save to file', () => {
        browser.url('https://webdriver.io/')
        const screenshot_1 = browser.takeElementScreenshot($('a.button=Get Started').elementId)
        const screenshot_2 = browser.takeElementScreenshot($('a.button=Support').elementId)
        // const screenshot_2 = $('a.button=Support').saveScreenshot('element_screenshot.png')
        expect(Buffer.from(screenshot_1, 'base64')).toEqual(Buffer.from(screenshot_2, 'base64'))
    })
});

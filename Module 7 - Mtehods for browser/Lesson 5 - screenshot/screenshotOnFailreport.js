const WDIOReporter = require('@wdio/reporter').default

class ScreenshotOnFailReport extends WDIOReporter {
    constructor(options){
        super(options)
        this.options = options
    }

    onTestFail(test) {
        browser.saveScreenshot(`${test.title}_failed.png`)
    }
}

module.exports = {
    ScreenshotOnFailReport: ScreenshotOnFailReport
}
const uuid = require('uuid');

describe('Open new tabs', () => {
    it('should open a new tab', () => {
        browser.url('https://wall.sli.do/event/uxq42ad8?section=cbbd9046-6ff0-4a18-99f0-6b848138c53e')

        browser.newWindow('https://app.sli.do/event/uxq42ad8/live/questions', {
            windowName: 'Sli.do window'
        })

        const msg = `My new question ${uuid.v4()}`
        // 268fe82e-2c49-49f9-83cc-3269251ba2df

        const field = $('#question-field')
        field.click()
        field.setValue(msg)
        $('button=Send').click()

        let clientTabs = clientChrome.getWindowHandles()
        let adminTabs = adminChrome.getWindowHandles()

        clientChrome.switchToWindow(clientTabs[0])
        adminChrome.switchToWindow(adminTabs[0])

        $(`div=${msg}`).waitForExist()

        clientChrome.switchToWindow(clientTabs[1])
        adminChrome.switchToWindow(adminTabs[1])
        clientChrome.closeWindow()
        adminChrome.closeWindow()
        
        let new_tabs = browser.getWindowHandles() // [1]
        expect(new_tabs.length).toBe(1)
        browser.debug()
    });

    it.only('shoud open two browser', () => {
        adminChrome.url('https://wall.sli.do/event/uxq42ad8?section=cbbd9046-6ff0-4a18-99f0-6b848138c53e')
        clientChrome.url('https://app.sli.do/event/uxq42ad8/live/questions')

        const msg = `My new question ${uuid.v4()}`

        clientChrome.$('#question-field').click()
        clientChrome.$('#question-field').setValue(msg)

        clientChrome.$('button=Send').click()

        adminChrome.$(`div=${msg}`).waitForExist()
        browser.debug()
    })
})

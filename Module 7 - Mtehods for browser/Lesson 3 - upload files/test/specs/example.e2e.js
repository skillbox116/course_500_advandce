const path = require('path')
const { pathExistsSync } = require('fs-extra')
const fs = require('fs')

describe('Upload and download files', () => {
    it('upload', () => {
        browser.url('/upload');
        const uploadFileName = 'upload.txt'
        
        const remoteFilePath = browser.uploadFile(path.join(process.cwd(), uploadFileName))

        $('#file-upload').setValue(remoteFilePath) // -> /usr/avorobey/skillbox/lesson3/upload.txt
        $('#file-submit').click()
        browser.debug()
    });

    it.only('download', () => {
        browser.url('./download');
        // global.downloadDir

        const fileName = 'some-file.txt'

        $(`*=${fileName}`).click()
        const filePath = path.join(global.downloadDir, fileName)
        browser.waitUntil(() => pathExistsSync(filePath), 10000)
        expect(fs.readFileSync(filePath, 'utf-8')).toContain('asdf')
    });
});

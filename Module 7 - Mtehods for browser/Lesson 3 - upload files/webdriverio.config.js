const path = require('path')
global.downloadDir = path.join(process.cwd(), '.tmp/chromeDownloadFolder')

exports.config = {
    specs: ['./test/specs/example.e2e.js'],
    automationProtocol: 'webdriver',
    exclude: [],
    path: '/wd/hub',
    maxInstances: 2,
    capabilities: [{
        browserName: 'chrome',
        'goog:chromeOptions': {
            prefs: {
                'download.default_directory': global.downloadDir
            }
        }
    }],
    baseUrl: 'https://the-internet.herokuapp.com',
    waitforTimeout: 10000, // ms
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,

    // Level of logging verbosity: trace | debug | info | warn | error | silent
    logLevel: 'debug',

    services: ['selenium-standalone'],
    // https://webdriver.io/docs/frameworks
    framework: 'mocha',
    // https://webdriver.io/docs/dot-reporter
    reporters: ['spec'],
    // http://mochajs.org/
    mochaOpts: {
        ui: 'bdd',
        timeout: 6000000
    }
}
